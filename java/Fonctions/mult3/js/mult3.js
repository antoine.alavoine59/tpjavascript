//fonction qui calcul les multiple de 3
function Mult3(prmMax) {
    let valreturn = "0";        //valeur de retour sur 0
    for (let index = 1; index < prmMax; index++) {
        if (index % 3 == 0) {
            valreturn = valreturn + "-" + index;
        }
    }
    return valreturn;
}
let valLim = 20;        //valeur limite mise a 20
console.log("Recherches des multiples de 3 :");
console.log("Valeur limite de la recherche : " + valLim);
console.log("Multiples de 3 : " + Mult3(valLim));
