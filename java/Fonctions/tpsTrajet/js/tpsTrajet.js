//calcul temps de trajet en sec
function calculerTempsParcoursSec(prmv, prmd) {
    let valreturn = prmd / prmv * 3600;
    return valreturn;
}
//conversion en h,m,s
function convertiseur(prmtempensec) {
    let h;      //heures
    let m;      //minutes
    let s;      //secondes
    let valreturn;      //valeur de retour
    h = Math.floor(prmtempensec / 3600);
    m = Math.floor((prmtempensec / 60) - (h * 60));
    s = Math.floor(prmtempensec - (m * 60) - (h * 3600))
    valreturn = h + "h " + m + "min " + s + "s";
    return valreturn;
}
let v = 90;     //vitesse
let d = 500;    //distance
console.log("Calcul du temps de parcours d'un trajet :");
console.log("       Vitesse moyenne (en km/h) :   " + v);
console.log("       Distance à parcourir (en km): " + d);
console.log("A " + v + " km/h, une distance de " + d + " km est parcourue en " + calculerTempsParcoursSec(v, d) + " s");        //affichage en s
tempensec = calculerTempsParcoursSec(v, d);     //conversion en h,m,s
console.log("A " + v + " km/h, une distance de " + d + " km est parcourue en " + convertiseur(tempensec));      //affichage en h,m,s
