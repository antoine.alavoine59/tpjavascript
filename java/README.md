# Tp JavaScript
## Mes TP en JavaScript

> - Auteur : Antoine Alavoine
> - Date de publication : 06/04/2021

* [1.Introduction HelloWorld](HelloWorld_JS/index.html)
* [2.Variables et Opérateurs]()
  * [2.1 Surface](Chapitre2/surface/index.html)
  * [2.1 IMC](Chapitre2/imc/index.html)
  * [2.1 Temperature](Chapitre2/temperature/index.html)
* [3.Les structures de contrôle]()
  * [3.1 Conjecture de Syracuse](Chapitre3/Suite_Syracuse/index.html)
  * [3.2 Calcul de Factorielle](Chapitre3/CalcFactorielle/index.html)
  * [3.3 Conversion Euro/Dollars](Chapitre3/ConvEuro_Dollars/index.html)
  * [3.4 Nombres Triples](Chapitre3/NbTriples/index.html)
  * [3.5 Suite de Fibonacci](Chapitre3/Fibonacci/index.html)
  * [3.6 Table de multiplication](Chapitre3/TblMult/index.html)
* [4.Les Fonctions]()
  * [4.1 Calculer et interpreter l'IMC](Fonctions/imc2/index.html) 
  * [4.2 Calcul du temps de parcours d'un trajet](Fonctions/tpsTrajet/index.html)
  * [4.3 Recherche du nombre de multiples de 3](Fonctions/mult3/index.html)